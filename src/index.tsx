import * as React from "react";
import * as ReactDOM from "react-dom";
import { DatePicker, message } from 'antd';

import { Hello } from "./components/Hello";
import App from "./App";

ReactDOM.render(
    <App />,
    document.getElementById("example")
);