const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');


module.exports = {
  // mode: "development",
  mode: "production",

  entry: {
    main: './src/index.tsx'
  },

  devtool: 'inline-source-map',


  // Enable sourcemaps for debugging webpack's output.
  devtool: "source-map",


  output: {
    filename: "[name].bundle.js",
    chunkFilename: '[name].bundle.js',
    path: path.resolve(__dirname, 'dist')
  },

  resolve: {
      // Add '.ts' and '.tsx' as resolvable extensions.
      extensions: [".ts", ".tsx", ".js"]
  },

  module: {
      rules: [
          {
              test: /\.ts(x?)$/,
              exclude: /node_modules/,
              use: [
                  {
                      loader: "ts-loader"
                  }
              ]
          },
          {
            test: /\.css$/i,
            use: ['style-loader', 'css-loader'],
          },
          // All output '.js' files will have any sourcemaps re-processed by 'source-map-loader'.
          {
              enforce: "pre",
              test: /\.js$/,
              loader: "source-map-loader"
          }
      ]
  },

  // When importing a module whose path matches one of the following, just
  // assume a corresponding global variable exists and use that instead.
  // This is important because it allows us to avoid bundling all of our
  // dependencies, which allows browsers to cache those libraries between builds.
  externals: {
      // "react": "React",
      // "react-dom": "ReactDOM"
  },

  plugins: [new HtmlWebpackPlugin({
    template: __dirname + '/index.html'
  })]

};