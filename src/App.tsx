import React, {Component, FormEvent} from 'react';
import {Task} from './models/task';
import {NewTaskForm} from './components/NewTaskForm';
import {TasksList} from './components/TaskList';
import { DatePicker } from 'antd';
// import './App.css';
import { Button } from 'antd'

interface State {
  newTask: Task,
  tasks: Task[]
}

export default class App extends Component<{}, State> {
  state = {
    newTask: {
      id: 1,
      name: ''
    },
    tasks: [{id: 10, name: "haha"}] as Task[]
  };
  
  render() {
    return (
      <div>
        <DatePicker size="small" /><br/>
        <br/>
        <br/>
        <Button type="primary" size="small">Primary hello</Button>
        <br/>
        <br/>
        <br/>
        <Button type="ghost" size="small">Secondary hello</Button>
        <h2>hello</h2>
        <NewTaskForm task={this.state.newTask} onAdd={this.addTask} onChange={this.handleTaskChange} />
        <br/>
        <TasksList tasks={this.state.tasks} onDelete={this.deleteTask} />
      </div>
    )
  }

  private addTask = (event: React.FormEvent<HTMLFormElement>) => {
    event.preventDefault();
  
    this.setState(previousState => ({
      newTask: {
        id: previousState.newTask.id + 1,
        name: ""
      },
      tasks: [...previousState.tasks, previousState.newTask]
    }));
  };

  private handleTaskChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    this.setState({
      newTask: {
        ...this.state.newTask,
        name: event.target.value
      }
    });
  };

  private deleteTask = (taskToDelete: Task) => {
    this.setState(previousState => ({
      tasks: [
        ...previousState.tasks.filter(task => task.id !== taskToDelete.id)
      ]
    }));
  };

};
